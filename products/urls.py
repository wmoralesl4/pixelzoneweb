

from django.urls import path
from .views import *
app_name = 'products'

urlpatterns = [
    path('d/', DesignListView.as_view(), name='design_list'),
    path('d/<str:pk>/', DesignDetailView.as_view(), name='design_detail'),
]
