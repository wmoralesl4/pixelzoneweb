from django.shortcuts import render
from .models import Design
from django.views.generic import ListView, DetailView

class DesignListView(ListView):
    model = Design
    template_name = 'products/design/design_list.html'
    context_object_name = 'playeras'

class DesignDetailView(DetailView):
    model = Design
    template_name = 'products/design/design_detail.html'
    context_object_name = 'playera'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_design = self.object  # El diseño actual (detalle)
        
        # Obtener los 3 diseños relacionados por categoría (tags)
        related_designs = Design.objects.filter(
            tags__in=current_design.tags.all()  # Diseños que compartan tags
        ).exclude(id=current_design.id).distinct()[:4]  # Excluir el diseño actual, limitar a 3

        # Agregar los diseños relacionados al contexto
        context['related_designs'] = related_designs
        return context