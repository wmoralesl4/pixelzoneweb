from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.contrib.auth import get_user_model
import requests
from django.core.files.base import ContentFile

User = get_user_model()

class MySocialAccountAdapter(DefaultSocialAccountAdapter):
    def save_user(self, request, sociallogin, form=None):
        user = sociallogin.user
        extra_data = sociallogin.account.extra_data
        picture_url = extra_data.get('picture')  # URL de la foto de perfil
        
        if picture_url:
            try:
                # Descargar la imagen desde la URL
                response = requests.get(picture_url)
                response.raise_for_status()
                
                # Obtener el nombre del archivo
                file_name = f"{user.username}_profile.jpg"

                # Guardar la imagen en el campo `photo`
                user.photo.save(file_name, ContentFile(response.content), save=False)
            except requests.RequestException as e:
                print(f"Error al descargar la imagen: {e}")
        
        user.save()  # Guardar los cambios en el usuario

        # Retornar el usuario existente si ya existe uno con el mismo email
        existing_user = User.objects.filter(email=user.email).first()
        if existing_user:
            sociallogin.state['process'] = 'connect'
            sociallogin.connect(request, existing_user)
            return existing_user

        return super().save_user(request, sociallogin, form)
    

    # class MySocialAccountAdapter(DefaultSocialAccountAdapter):
    # def pre_social_login(self, request, sociallogin):
    #     # Si ya hay un usuario con este correo, vincularlo
    #     email = sociallogin.user.email
    #     print(sociallogin.user)
    #     if email:
    #         try:
    #             # Buscar usuario existente por correo
    #             user = User.objects.get(email=email)
    #             sociallogin.connect(request, user)
    #         except User.DoesNotExist:
    #             # Permitir que se cree un nuevo usuario
    #             sociallogin.user.email = email
    #             sociallogin.user.username = email.split('@')[0]  # Usa el correo como base del username
    #             sociallogin.user.save()

    # def save_user(self, request, sociallogin, form=None):
    #     """
    #     Sobrescribe el método save_user para evitar duplicados
    #     """
    #     user = sociallogin.user
    #     existing_user = User.objects.filter(email=user.email).first()
    #     if existing_user:
    #         # Retornar el usuario existente en lugar de crear uno nuevo
    #         sociallogin.state['process'] = 'connect'
    #         sociallogin.connect(request, existing_user)
    #         return existing_user
    #     return super().save_user(request, sociallogin, form)
